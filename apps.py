from django.apps import AppConfig


class CopyboardConfig(AppConfig):
    name = 'copyboard'

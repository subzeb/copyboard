from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from .models import CopyPaste
from .forms import ButtonForm


@login_required
def front(request):
    button_list = CopyPaste.objects.filter(owner=request.user).order_by('-title')
    return render(request, 'copyboard/button_list.html', {'buttons': button_list})


@login_required
def edit_button(request, url):
    button = get_object_or_404(CopyPaste, url=url)
    if request.method == "POST":
        form = ButtonForm(request.POST, instance=button)
        if form.is_valid():
            button = form.save(commit=False)
            button.owner = request.user
            button.save()
            return redirect('button_list')
    else:
        form = ButtonForm(instance=button)
    return render(request, 'copyboard/button_edit.html', {'form': form})


@login_required
def new_button(request):
    if request.method == "POST":
        form = ButtonForm(request.POST)
        if form.is_valid():
            button = form.save(commit=False)
            button.owner = request.user
            button.save()
            return redirect('button_list')
    else:
        form = ButtonForm()
    return render(request, 'copyboard/button_edit.html', {'form': form})


@login_required
def delete_button_ask(request, url):
    button = get_object_or_404(CopyPaste, url=url, owner=request.user)
    if request.user == button.owner:
        return render(request, 'copyboard/button_delete.html', {'button': button})
    else:
        return redirect('button_list')


@login_required
def delete_button(request, url):
    button = get_object_or_404(CopyPaste, url=url, owner=request.user)
    if request.user == button.owner:
        button.delete()
    return redirect('button_list')

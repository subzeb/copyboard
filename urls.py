from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.front, name='button_list'),
    url(r'^edit/(?P<url>[\D\d]+)$', views.edit_button, name='edit_button'),
    url(r'^new/$', views.new_button, name='new_button'),
    url(r'^delete/submit/(?P<url>[\D\d]+)$', views.delete_button, name='delete_button'),
    url(r'^delete/(?P<url>[\D\d]+)$', views.delete_button_ask, name='delete_button_ask'),

]
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


class CopyPaste(models.Model):
    title = models.CharField(max_length=40, null=False, blank=False)
    body = models.TextField(blank=False, null=False)
    url = models.SlugField(max_length=40, blank=True)
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    shared = models.BooleanField(default=False)
    uses = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if not self.id:
            self.url = slugify(self.title)
        super(CopyPaste, self).save(*args, **kwargs)

    def update(self, text):
        self.body = str(text)
        self.save()

    def __str__(self):
        return self.title

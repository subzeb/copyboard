from django import forms
from.models import CopyPaste


class ButtonForm(forms.ModelForm):

    class Meta:
        model = CopyPaste
        fields = ('title', 'body',)